output "instance_id" {
  description = "Id of backend instance"
  value       = aws_instance.instance.id
}

# output "backend_sg_id" {
#   description = "Id of backend-sg"
#   value       = aws_security_group.backend_sg[0].id
# }

# output "backend_instance_id" {
#   description = "Id of backend instance"
#   value       = aws_instance.backend[0].id
# }

# output "backend_ami_id" {
#   description = "Id of backend instance"
#   value       = local.ami_id
# }

# output "aws_ec2_backend_instance_enabled" {
#   description = "Id of backend instance"
#   value       = local.aws_ec2_backend_instance_enabled
# }

# # for info file
# output "backend_public_ip" {
#   description = "Id of bastion instance"
#   value       = aws_instance.backend[0].public_ip
# }

# output "backend_private_ip" {
#   description = "Id of backend instance"
#   value       = aws_instance.backend[0].private_ip
# }

# output "backend_ssh_keyname" {
#   description = "Id of backend instance"
#   value       = aws_key_pair.backend_generated_key[0].key_name
# }

# output "backend_ami_name" {
#   description = "Id of bastion instance"
#   value       = data.aws_ami.base_ami[0].name
# }

# output "backend_instance_type" {
#   description = "Id of bastion instance"
#   value       = var.instance_type
# }
