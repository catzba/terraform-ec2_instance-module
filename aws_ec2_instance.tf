#-----------------------------------------------------------------------------------------------------
# EC2 Instance
#-----------------------------------------------------------------------------------------------------

resource "aws_instance" "instance" {
  ami                     = data.aws_ami.centos7_ami.id
  instance_type           = var.instance_type
  user_data               = var.user_data # script
  get_password_data       = var.get_password_data
  monitoring              = var.monitoring
  subnet_id               = var.subnet_private_id
  key_name                = aws_key_pair.instance_generated_key.key_name
  disable_api_termination = var.disable_api_termination
  # iam_instance_profile = var.iam_instance_profile # dont forget add variable

  root_block_device {
    delete_on_termination = var.root_delete_on_termination
    encrypted             = var.root_encrypted
    volume_size           = var.root_volume_size
    volume_type           = var.root_volume_type
  }

  volume_tags = {
    Name        = "${var.project_name}-${var.environment}-${var.instance_name}"
    Project     = var.project_name
    Environment = var.environment
  }

  credit_specification {
    cpu_credits = "standard"
  }

  vpc_security_group_ids = [
    var.instance_sg_id
  ]

  tags = {
    Name        = "${var.project_name}-${var.environment}-${var.instance_name}"
    Project     = var.project_name
    Environment = var.environment
  }
}
