#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
  default = "default"
}

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# INPUT
#-----------------------------------------------------------------------------------------------------

variable "ssh_key_path" {
  type = string
  default = "~/.ssh/"
}

variable "instance_sg_id" {
  type = string
}

variable "instance_name" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# VPC VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "vpc_id" {
  type = string
}

variable "subnet_public_id" {
  type = string
  default = "default"
}

variable "subnet_private_id" {
  type = string
  default = "default"
}


#-----------------------------------------------------------------------------------------------------
# EC2 INSTANCE VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "instance_type" {
  type        = string
  default     = "t2.micro"
}

variable "user_data" {
  description = "The user data to provide when launching the instance"
  type        = string
  default     = ""
}

variable "get_password_data" {
  description = "The user data to provide when launching the instance"
  type        = bool
  default     = false
}

variable "monitoring" {
  description = "If true, the launched EC2 instance will have detailed monitoring enabled"
  type        = bool
  default     = true
}

variable "disable_api_termination" {
  description = "If true, enables EC2 Instance Termination Protection"
  type        = bool
  default     = false
}

#-----------------------------------------------------------------------------------------------------
# EC2 ROOT VOLUME VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "root_delete_on_termination" {
  type        = bool
  default     = true
}
variable "root_encrypted" {
  type        = bool
  default     = false
}

variable "root_volume_size" {
  type        = number
  default     = 10
}

variable "root_volume_type" {
  type        = string
  default     = "gp2"
}