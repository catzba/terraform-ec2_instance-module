#-----------------------------------------------------------------------------------------------------
# AMI Centos 7 find
#-----------------------------------------------------------------------------------------------------

data "aws_ami" "centos7_ami" {
  most_recent = true
  owners      = ["679593333241"] # Centos.org

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


