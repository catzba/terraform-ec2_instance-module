#-----------------------------------------------------------------------------------------------------
# SSH instance
#-----------------------------------------------------------------------------------------------------

# Generete SSH key for instance admin access 
resource "tls_private_key" "instance" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# upload public key to AWS key pair
resource "aws_key_pair" "instance_generated_key" {
  key_name   = "${var.project_name}-${var.environment}-${var.instance_name}"
  public_key = tls_private_key.instance.public_key_openssh
}

# set path for save keys to .ssh/project_name/envarionment-eu-central-1-key-name.pem
locals {

  private_key_filename_instance = format(
    "%s/%s/%s%s",
    pathexpand(var.ssh_key_path),
    var.project_name,
    aws_key_pair.instance_generated_key.key_name,
    ".pem",
  )

}

#-----------------------------------------------------------------------------------------------------
# SSH key files
#-----------------------------------------------------------------------------------------------------

# Save private key to file
resource "local_file" "instance_private_key_pem" {
  depends_on = [tls_private_key.instance]
  content    = tls_private_key.instance.private_key_pem
  filename   = local.private_key_filename_instance

  provisioner "local-exec" {
    # Fix permissions for key
    command = "chmod 600 ${local.private_key_filename_instance}"
  }
}

#-----------------------------------------------------------------------------------------------------
# Save to S3
#-----------------------------------------------------------------------------------------------------


# Upload SSH private key 
# resource "aws_s3_bucket_object" "instance_private_key_pem" {
#   bucket  = var.aws_s3_credentials_bucket_name
#   key     = "${var.project_name}-${var.environment}-${var.instance_name}.pem"
#   content = tls_private_key.instance.private_key_pem
# }

